<?php

namespace App\Http\Controllers\Operative;

use App\Http\Controllers\Controller;
use App\Models\Admin\Headquarter;
use App\Models\Admin\Person;
use App\Models\Operative\CenterAreas;
use App\Models\Operative\Relationship;
use App\Models\Operative\VisitAccess;
use App\Models\Operative\VisitAccessAudit;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Workflow\Exception\TransitionException;
use Symfony\Component\Workflow\Transition;

class VisitAccessController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role');
        $this->middleware('permission');
//        $this->middleware('permission_menu');
    }

    /**
     * Genera JSON en el formato que espera el helper de Select2
     *
     * @param $person
     * @return array
     */
    public function mapForSelect2($person) {
        return [
            'id' => $person->peid,
            'text' => $person->full_name . " ($person->penumr_idntfccn)"
        ];
    }

    /**
     * Genera una JSON para pasarle a Select2 de modo que preseleccione una persona.
     *
     * @param $ids
     * @return false|string
     */
    public function generateSelectedPersonValue($ids) {
        if (!is_iterable($ids)) {
            $ids = [$ids];
        }
        $result = [];
        foreach ($ids as $id) {
            $person = Person::query()->find($id);
            if ($person !== null) {
                $result[] = $this->mapForSelect2($person);
            }
        }
        return json_encode($result);
    }

    /**
     * @param $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function generateValidator($data) {
        $v = Validator::make($data, [
            'visitado' => 'required',
            'vifech' => 'required',
            'hora_llegada' => 'required',
            'hora_salida' => 'required',
        ], [
            'visitado.required' => 'Escoger si el visitado es un Centro o un Adolescente.',
            'insed.required' => 'Escoger un Centro a visitar.',
            'viigid.required' => 'Escoger un Adolescente a visitar.',
            'visitante.required' => 'Escoger al menos un visitante.',
            'vifech.required' => 'Ingresar la fecha de la visita.',
            'hora_llegada.required' => 'Ingresar la hora de llegada de la visita.',
            'hora_salida.required' => 'Ingresar la hora de salida de la visita',
            'diferencia_horas.required' => 'La hora de salida no puede ser menor a la hora de entrada.'
        ]);
        $v->sometimes('insed', 'required', function ($input) {
            return $input->visitado === 'centro';
        });
        $v->sometimes('viigid', 'required', function ($input) {
            return $input->visitado === 'adolescente';
        });
        $v->sometimes('visitante', 'required', function ($input) {
            return empty($input->visitantes) && empty($input->visitantes_iei) && empty($input->visitantes_pext);
        });
        $v->sometimes('diferencia_horas', 'required', function ($input) {
           $hora_salida = strtotime($input->hora_salida);
           $hora_llegada = strtotime($input->hora_llegada);
           return $hora_salida <= $hora_llegada;
        });
        return $v;
    }

    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $workflowName = 'visit_access';
        $workflowConfig = \Config::get('workflow');
        $workflowStates = array_map(function ($v) {
            return $v['label'];
        }, data_get($workflowConfig, "$workflowName.template_config.places"));

        $searchTerms = $request->get('searchterms');
        $selectedWorkflowState = $request->get('workflowstate');

        $menuDefault = ['' => '-- SELECCIONE --'];

        /** @var User $user */
        $user = Auth::user();
        $nombreSede = null;
        if ($user->hasRole('Admin')) {
            $sedes = $menuDefault + Headquarter::query()
                    ->pluck('tpdescrpcn', 'tpcodg')
                    ->toArray();
            $selectedSede = $request->get('insed');
        } else {
            $sedes = null;
            $selectedSede = $user->person->sede_id;
            $nombreSede = $user->person->centro->tpdescrpcn;
        }

        $visitsQuery = VisitAccess::query();
        if (!empty($selectedWorkflowState)) {
            $visitsQuery->whereIn('estado', $selectedWorkflowState);
        }
        if (!empty($selectedSede)) {
            $visitsQuery->whereIn('insed', [$selectedSede]);
        }

        if (!empty($searchTerms)) {
            $visitsQuery->leftJoin('sepersns', 'opvists.vipeid', '=', 'sepersns.peid');
            $visitsQuery->where(function (Builder $query) use ($searchTerms) {
                $query->orWhere('sepersns.penumr_idntfccn', 'like', '%' . $searchTerms . '%');
                $query->orWhere('sepersns.peprimr_nombr', 'like', '%' . $searchTerms . '%');
                $query->orWhere('sepersns.pesegnd_nombr', 'like', '%' . $searchTerms . '%');
                $query->orWhere('sepersns.peprimr_aplld', 'like', '%' . $searchTerms . '%');
                $query->orWhere('sepersns.pesegnd_aplld', 'like', '%' . $searchTerms . '%');
            });
        }
        $visits = $visitsQuery->paginate(10);
        return View('operative.visit_access.index',
            compact('visits', 'workflowName', 'workflowStates', 'selectedWorkflowState', 'searchTerms', 'sedes', 'selectedSede', 'nombreSede')
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        $areas = CenterAreas::query()
            ->where('arestd', '=', 1)
            ->pluck('arnombr', 'arid');

        $menuDefault = ['' => '-- SELECCIONE --'];
        /** @var User $user */
        $user = Auth::user();

        if ($user->hasRole('Admin')) {
            $sedes = $menuDefault + Headquarter::query()
                    ->pluck('tpdescrpcn', 'tpcodg')
                    ->toArray();
        } else {
            $nombreSede = $user->person->centro->tpdescrpcn;
            $selectedSede = $user->person->centro->tpcodg;
            $sedes = null;
        }

        $adolescentesSelectedJson = $this->generateSelectedPersonValue($request->old('viigid'));
        $visitantesSelectedJson = $this->generateSelectedPersonValue($request->old('visitantes'));
        $visitantesIEISelectedJson = $this->generateSelectedPersonValue($request->old('visitantes_iei'));
        $visitantesPersonalExternoSelectedJson = $this->generateSelectedPersonValue($request->old('visitantes_pext'));

        return View('operative.visit_access.create',
            compact('areas',
                'sedes',
                'nombreSede',
                'selectedSede',
                'adolescentesSelectedJson',
                'visitantesSelectedJson',
                'visitantesIEISelectedJson',
                'visitantesPersonalExternoSelectedJson'
            )
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $v = $this->generateValidator($request->all());
        $v->validate();

        $visitAccess = new VisitAccess();

        /** @var User $user */
        $user = Auth::user();

        if ($user->hasRole('Admin')) {
            $sede = $request->get('insed');
        } else {
            $sede = $user->person->sede_id;
        }

        $visitado = $request->get('visitado');
        $visita_centro = $visitado == 'centro' ? true : false;

        $visitAccess->fill([
            'viigid' => $request->get('viigid'),
            'vifech' => $request->get('vifech'),
            'hora_llegada' => $request->get('hora_llegada'),
            'hora_salida' => $request->get('hora_salida'),
            'vifech_regstr' => $request->get('vifech_regstr'),
            'viusr_regstr' => $user->usid,
            'insed' => $sede,
            'visita_centro' => $visita_centro,
        ]);

        DB::transaction(function () use ($request, $visitAccess) {
            // mark model with initial workflow state
            $workflow = \Workflow::get($visitAccess, 'visit_access');
            $workflow->getMarking($visitAccess);
            // model needs to exist in db first in order to attach m2m related models
            $visitAccess->save();
            $visitAccess->allowedAreas()->attach($request->get('allowedAreas'));
            $visitAccess->visitantes()->attach($request->get('visitantes'));
            $visitAccess->visitantesIEI()->attach($request->get('visitantes_iei'));
            $visitAccess->visitantesPersonalExterno()->attach($request->get('visitantes_pext'));
        });

        return redirect()->route('visit_access.index');
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     * @throws \Exception
     */
    public function edit($id)
    {
        /** @var VisitAccess $visit */
        $visit = VisitAccess::query()->find($id);

        // Para adolescentes como la relacion es BelongsTo y retorna un solo valor, no usamos map().
        $adolescente_id = $visit->adolescente->peid ?? null;

        $adolescentesSelectedJson = $this->generateSelectedPersonValue($adolescente_id);
        $visitantesSelectedJson = $this->generateSelectedPersonValue($visit->visitantes->pluck('peid'));
        $visitantesIEISelectedJson = $this->generateSelectedPersonValue($visit->visitantesIEI->pluck('peid'));
        $visitantesPersonalExternoSelectedJson = $this->generateSelectedPersonValue($visit->visitantesPersonalExterno->pluck('peid'));

        $menuDefault = ['' => '-- SELECCIONE --'];
        /** @var User $user */
        $user = Auth::user();
        if ($user->hasRole('Admin')) {
            $sedes = $menuDefault + Headquarter::query()
                    ->pluck('tpdescrpcn', 'tpcodg')
                    ->toArray();
        } else {
            $sedes = null;
            $nombreSede = $user->person->centro->tpdescrpcn;
            $selectedSede = $user->person->centro->tpcodg;        }

        $areas = CenterAreas::query()
            ->where('arestd', '=', 1)
            ->pluck('arnombr', 'arid');
        $areaIds = $visit->allowedAreas()->pluck('arid')->toArray();

        return View('operative.visit_access.edit', compact(
            'visit',
            'areas',
            'areaIds',
            'sedes',
            'adolescentesSelectedJson',
            'visitantesSelectedJson',
            'visitantesIEISelectedJson',
            'visitantesPersonalExternoSelectedJson',
            'nombreSede',
            'selectedSede'
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $v = $this->generateValidator($request->all());
        $v->validate();

        /** @var VisitAccess $visit */
        $visit = VisitAccess::query()->find($id);

        /** @var User $user */
        $user = Auth::user();
        if ($user->hasRole('Admin')) {
            $sede = $request->get('insed');
        } else {
            $sede = $user->person->sede_id;
        }
        $visit->insed = $sede;

        $visitado = $request->get('visitado');
        $visita_centro = $visitado == 'centro' ? true : false;

        $visit->fill([
            'viigid' => $request->get('viigid'),
            'vifech' => $request->get('vifech'),
            'hora_llegada' => $request->get('hora_llegada'),
            'hora_salida' => $request->get('hora_salida'),
            'vifech_regstr' => $request->get('vifech_regstr'),
            'viusr_regstr' => $user->usid,
            'insed' => $sede,
            'visita_centro' => $visita_centro,
        ]);

        $visit->allowedAreas()->sync($request->get('allowedAreas'));
        $visit->visitantes()->sync($request->get('visitantes'));
        $visit->visitantesIEI()->sync($request->get('visitantes_iei'));
        $visit->visitantesPersonalExterno()->sync($request->get('visitantes_pext'));

        $visit->save();

        return redirect()->route('visit_access.index');
    }

    /**
     * @param $id
     * @param $transition
     * @return \Illuminate\View\View
     */
    public function workflowTransition($id, $transition)
    {
        $workflowName = 'visit_access';
        $workflowConfig = \Config::get('workflow');
        $config = data_get($workflowConfig, "$workflowName.template_config.transitions");
        $transitionClass = data_get($config, "$transition.class");
        $transitionLabel = data_get($config, "$transition.label");

        return View('operative.visit_access.workflowTransition', compact(
            'id', 'transition', 'transitionLabel', 'transitionClass'
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @param $transitionName
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function workflowTransitionApply(Request $request, $id, $transitionName)
    {
        $data = $request->validate([
            'comentario' => 'required',
        ]);

        /** @var VisitAccess $visit */
        $visit = VisitAccess::query()->find($id);

        $workflowName = 'visit_access';
        $workflow = \Workflow::get($visit, $workflowName);

        $transitions = $workflow->getEnabledTransitions($visit);

        /** @var Transition $transition */
        $transition = array_first($transitions, function (Transition $value, $key) use ($transitionName) {
            return $value->getName() === $transitionName;
        });
        $endState = implode(',', $transition->getTos());

        /** @var User $user */
        $user = Auth::user();

        $visitAudit = new VisitAccessAudit();
        $visitAudit->campo = 'estado';
        $visitAudit->val_prev = $visit->estado;
        $visitAudit->val_nuevo = $endState;
        $visitAudit->usuario_id = $user->getAuthIdentifier();
        $visitAudit->usuario_nombre = $user->uslogn;
        $visitAudit->comentario = $data['comentario'];

        try {
            $workflow->apply($visit, $transitionName);
            $visit->audits()->save($visitAudit);
            $visit->save();
        } catch (TransitionException $exception) {
            // TODO: mandar mensaje de error para transicion invalida.
            throw $exception;
        } catch (\Exception $exception) {
            // TODO: mandar mensaje de error para excepcion general.
            throw $exception;
        }

        return redirect()->route('visit_access.index', ['#visita-detalle-' . $id]);
    }


}