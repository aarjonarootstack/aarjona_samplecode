* IdentificacionFuncionario.vue: Small widget VueJS component with a couple dependent fields and some Bootstrap markup.

* visits.js: Misc. jQuery code for tasks like adding an animation to a Bootstrap component, applying Select2 to some
             fields and managing dependent field form state for a web app managing visitors.