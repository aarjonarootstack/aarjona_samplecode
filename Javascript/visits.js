const {
  ajaxSelection,
  templateResult,
  templateSelection,
} = require("../utils/helper");

// girar el chevron al expandir registro en tabla
$(document).ready(function () {
  $(".visita-detalle").on("show.bs.collapse hidden.bs.collapse", function () {
    const visitaId = $(this).data("visita-id");
    $(".visita-detalle-chevron-" + visitaId)
      .toggleClass("fa-chevron-circle-right fa-chevron-circle-down");
  });

  $(".visita-detalle-link").on("click", function (e) {
    e.preventDefault();
  });

  // manejar la apertura de panel de detalle tanto en carga inicial como cuando cambia el hash
  function openDetailsPanel() {
    if (location.hash !== "") {
      const selector = location.hash.replace("#", ".");
      $(selector).collapse("show");
    }
  }
  $(window).on("hashchange", function () {
    openDetailsPanel();
  });
  openDetailsPanel();

  function applySelect2(selector, placeholder = "") {
    if (selector.length) {
      selector.select2({
        tags: false,
        placeholder: placeholder,
        minimumInputLength: 1,
        ajax: ajaxSelection(selector),
        templateResult: templateResult,
        templateSelection: templateSelection,
        escapeMarkup: (m) => { return m; },
      }).defaultSelectedItem();
    }
  }

  function processVisitanteState()
  {
    let visitado = $("input[name='visitado']:checked").val();
    let selectedAdolescente = adolescentesSelect.find(":selected").val();

    switch (visitado) {
    case "centro":
      // Cuando se visita un centro, se selecciona entre personal externo y del iei, no familiares.
      centroSelect.prop("disabled", false);
      adolescentesSelect.prop("disabled", true);

      visitantesSelect.prop("disabled", true);
      visitantesIEISelect.prop("disabled", false);
      visitantesPersonalExternoSelect.prop("disabled", false);

      break;
    case "adolescente":
      // Cuando se visita a un adolescente se selecciona entre personal externo y familiares
      centroSelect.prop("disabled", true);
      adolescentesSelect.prop("disabled", false);

      // al seleccionar un adolescente se ubica el id en una propiedad del
      // menu de familiares de modo que se filtre por ese adolescente.
      if (selectedAdolescente !== undefined) {
        visitantesSelect.data("parent-id", selectedAdolescente);
        visitantesSelect.prop("disabled", false);
      }
      visitantesIEISelect.prop("disabled", true);
      visitantesPersonalExternoSelect.prop("disabled", false);

      break;
    default:
      // por defecto se deshabilitan todos los campos
      centroSelect.prop("disabled", true);
      adolescentesSelect.prop("disabled", true);

      visitantesSelect.prop("disabled", true);
      visitantesIEISelect.prop("disabled", true);
      visitantesPersonalExternoSelect.prop("disabled", true);

      break;
    }
  }

  const vaForm = $("#va-form-create-edit");
  const centroSelect = vaForm.find("#insed");
  const adolescentesSelect = vaForm.find("#viigid");
  const visitantesSelect = vaForm.find("#visitantes\\[\\]");
  const visitantesIEISelect = vaForm.find("#visitantes_iei\\[\\]");
  const visitantesPersonalExternoSelect = vaForm.find("#visitantes_pext\\[\\]");

  applySelect2(adolescentesSelect, "Buscar Adolescentes");
  applySelect2(visitantesSelect, "Buscar Familiares");
  applySelect2(visitantesIEISelect, "Buscar Personal IEI");
  applySelect2(visitantesPersonalExternoSelect, "Buscar Personal Externo");

  vaForm.on("change", processVisitanteState);
  adolescentesSelect.on("change", processVisitanteState);
  processVisitanteState();


});