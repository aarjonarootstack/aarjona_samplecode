* user_migrate.inc: User migration sample from a suite using the migrate module.
It includes user picture migration as well as Organic Groups memberships via a custom query.

* voa_fair_rent_cases.module: part of a large module implementing a webservice-based ajax interface.
As the application grew, most functionality was refactored into separate files for forms, handlers, helpers, etc although
some remain in the .module due to in-progress issues with quirks of code inclusion and ajax callbacks.

* stri_scientist.module: part of a module implementing a block that shows some data pulled from an API, also contains
some form modifications and database query api usage.

* rkym_drush.drush.inc: custom drush command to perform a file migration.

* CdcfViewsReferenceHardcodedPresetsWidget.php: Drupal 8 symfony-based FieldWidget plugin used in a sidebar module.