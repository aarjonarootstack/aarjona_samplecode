<?php

namespace Drupal\cdcf_sidebar\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\viewsreference\Plugin\Field\FieldWidget\ViewsReferenceTrait;

/**
 * Plugin variant of the select widget but with hardcoded presets when in end-user land.
 *
 * @FieldWidget(
 *   id = "cdcf_viewsreference_hardcoded_presets",
 *   label = @Translation("CDCF Views Reference hardcoded preset values"),
 *   description = @Translation("A widget with hardcoded default values from the field definition."),
 *   field_types = {
 *     "viewsreference"
 *   }
 * )
 */
class CdcfViewsReferenceHardcodedPresetsWidget extends OptionsSelectWidget {

  use ViewsReferenceTrait;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $select_element['target_id'] = parent::formElement($items, $delta, $element, $form, $form_state);

    $select_element = $this->fieldElement($select_element, $items, $delta);
    $select_element['target_id']['#multiple'] = FALSE;
    if (!$this->isDefaultValueWidget($form_state)) {
      $selected_views = $items->getSetting('preselect_views');
      $selected_views = array_diff($selected_views, array("0"));
      $selected_views = $this->getViewNames($selected_views);
      if (count($selected_views) >= 1) {
        $first_option = array("- None -");
        $select_element['target_id']['#options'] = array_merge($first_option, $selected_views);
      }
      else {
        $select_element['target_id']['#empty_option'] = '- None -';
      }

      // When the widget is used in the field configuration we act like the regular select widget.
      // If we're in actual field use, we display the hidden fields instead.
      $select_element = array(
        'target_id' => array(
          '#type' => 'hidden',
          '#default_value' => current($select_element['target_id']['#default_value'])
        ),
        'display_id' => array(
          '#type' => 'hidden',
          '#default_value' => $select_element['display_id']['#default_value']
        ),
        'argument' => array(
          '#type' => 'hidden',
          '#default_value' => $select_element['argument']['#default_value'],
        ),
        'title' => array(
          '#type' => 'hidden',
          '#default_value' => $select_element['title']['#default_value'],
        ),
      );
    }

    return $select_element;
  }

}
